package br.com.si.entidades;

import java.io.Serializable;

/**
 * Enum que presenta o naipe de uma carta de baralho
 */
public enum Naipe implements Serializable {
    OURO {
        @Override
        public String toString() {
            return "O";
        }
    }, ESPADAS {
        @Override
        public String toString() {
            return "E";
        }
    }, COPAS {
        @Override
        public String toString() {
            return "C";
        }
    }, PAUS {
        @Override
        public String toString() {
            return "P";
        }
    }
}
