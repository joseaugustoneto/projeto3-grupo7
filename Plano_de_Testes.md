# **Plano de Testes Truco**  

# **Objetivo**  

Os testes tem o objetivo de identificar possíveis falhas e assim prevenir de erros.

# **Testes**  

- ** Execucao e Compilação **  
Conseguir  compilar o jar entidades primeiro e referenciar aos outros jars. 
Resultado esperado: conseguir a complicação e referência
Possível falha: não compilar o jar entidades, não conseguir referenciar
Resultado: compilação do jar e referências corretas

- ** Envio de mensagens **  
Conseguir enviar mensagens das referencias e comandos ao servidor e receber retorno
Resultado esperado: servidor retornar mensagem de recebido e conectado ou o contrario
Possível falha: não conectar e não informar ao usuário, servidor não receber comando
Resultado: comandos e mensagens recebidas e enviadas corretamente

- ** Pesquisa de comandos **  
Com o comando .ajuda poder listar todos os comandos existentes. Ao selecionar um dos comandos realização ação correta.
Resultado esperado: com o comando .ajuda listar os comandos e chamar métodos corretos
Possível falha: não abrir a lista de comando, executar um comando e o jogo fazer outra ação
Resultado: lista abrindo e comandos chamando métodos corretos

- ** Embaralhar **  
Usar vetores e seleção randomica para as cartas.
Resultado esperado: Ter cartas misturadas de formas diferentes.
Possível falha: Ter o mesmo conjunto de cartas sempre. Ter cartas repetidas
Resultado: as cartas do baralho são colocadas em uma ordem aleatória e é aplicado na lista de cartas. As cartas já estão criadas nas
classes de entidades do jogo.

- ** Escolha de cartas **  
De acordo com o comando de descartar permitir o jogador selecionar a carta e pontuar de acordo com as outras descartadas dos demais jogadores.
Resultado esperado: mostrar a lista de cartas que o jogador tem, retirar a descartada da lista, atribuir o ponto de acordo com as outras jogadas
Possível falha: não retirar a carta e deixar jogar de novo, mostrar as cartas que o jogadore tem erradas, atribuir os pontos errados
Resultado: lista de carta mostrada corretamente e pontuação também

- ** Pontualizar cartas e jogadores **  
A pontuação das cartas são definidas nas classes entidade. Os jogadores devem ser pontuados de acordo com os descartes de cartas das cartas que foram dadas a eles.
Resultado esperado: pontuar carta descartada
Possível falha: somar pontos incorretamente, atribuir valor errado a carta descartada
Resultado: pontuação feita corretamente e mostrado ao jogador por mensagem

- ** Finalizar jogo **  
Ao jogador ou dupla atingir a pontuação, finalizar  jogo e mostrar vencedor
Resultado esperado: mostrar mensagem do jogador que venceu
Possível falha: calcular pontuação errada e mostrar vencedor errado, não finalizar o jogo ao atingir pontuação
Resultado: pontuação e vencedor corretamente mostrados