package br.com.si.cliente;

import br.com.si.entidades.ServicoTruco;
import java.awt.event.ActionEvent;
import java.net.URL;
import static javafx.application.Platform.exit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class ClienteTruco extends JFrame {

    private Integer identificador;
    private final TelaJogo tela = new TelaJogo();
    private String nomeJogador;
    private String historicoMensagem = "";
    private boolean duplaSetada = false;

    public Integer getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public void setNomeJogador(String nome) {
        this.nomeJogador = nome;
    }

    public String getNomeJogador() {
        return this.nomeJogador;
    }

    /**
     * Configura e exibe o formulário na tela
     */
    public void showForm() {
        tela.setVisible(true);
        tela.labelNomeJogador.setText(nomeJogador);
        getServico().executarAcao(identificador, ".ajuda");

        definirAcoes();
        buscarAtualizacao();
    }

    /**
     * Define as ações dos botões da tela
     */
    private void definirAcoes() {
        //Adicionando um observador
        tela.buttonExecutar.addActionListener((ActionEvent e) -> {
            String comando;
            comando = tela.textFieldComando.getText();
            tela.textFieldComando.setText("");
            executarComandoServidor(comando);
        });
        
        tela.mnCriarJogo2Jogadores.addActionListener((ActionEvent e) -> {
            String comando;
            comando = ".criar parametroNaoUtilizado1 2 ";
            tela.textFieldComando.setText("");
            executarComandoServidor(comando);
        });
        
        tela.mnCriarJogo4Jogadores.addActionListener((ActionEvent e) -> {
            String comando;
            comando = ".criar parametroNaoUtilizado1 4 ";
            tela.textFieldComando.setText("");
            executarComandoServidor(comando);
        });
        
        tela.mnEntrarJogo.addActionListener((ActionEvent e) -> {
            String comando;
            comando = ".entrar parametroNaoUtilizado1";
            tela.textFieldComando.setText("");
            executarComandoServidor(comando);
        });
        
        tela.mnIniciarJogo.addActionListener((ActionEvent e) -> {
            String comando;
            comando = ".iniciar";
            tela.textFieldComando.setText("");
            executarComandoServidor(comando);
        });
        
        tela.btCarta1.addActionListener((ActionEvent e) -> {
            if (tela.btCarta1.getText().equals(""))
                exit();
            
            String comando = ".descartar " + tela.btCarta1.getText();
            executarComandoServidor(comando);
            habilitarBotoesCartasTela();
        });
        
        tela.btCarta2.addActionListener((ActionEvent e) -> {
            if (tela.btCarta2.getText().equals(""))
                exit();
            
            String comando = ".descartar " + tela.btCarta2.getText();
            executarComandoServidor(comando);
            habilitarBotoesCartasTela();
        });
        
        tela.btCarta3.addActionListener((ActionEvent e) -> {
            if (tela.btCarta3.getText().equals(""))
                exit();
            
            String comando = ".descartar " + tela.btCarta3.getText();
            executarComandoServidor(comando);
            habilitarBotoesCartasTela();
        });
        
        tela.btAtualizarPontuacao.addActionListener((ActionEvent e) -> {
            requisitarPontuacaoAtualizada();
        });
        
        tela.btTrucar.addActionListener((ActionEvent e) -> {
            String comando = ".trucar";
            executarComandoServidor(comando);
        });
        
        tela.btAceitar.addActionListener((ActionEvent e) -> {
            String comando = ".aceitar";
            executarComandoServidor(comando);
        });
        
        tela.btAumentar.addActionListener((ActionEvent e) -> {
            String comando = ".aumentar";
            executarComandoServidor(comando);
        });
    }
    
    private void executarComandoServidor(String comando){
        try {
            ServicoTruco s = getServico();
            s.executarAcao(identificador, comando);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Não foi possível executar: " + ex.getMessage());
        } 
    }

    public ClienteTruco() {

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ClienteTruco c = new ClienteTruco();
        if (c.getServico() != null) {
            try {
                c.nomeJogador = JOptionPane.showInputDialog(c, "Digite seu nome");
                c.setIdentificador(c.getServico().gerarIdentificador(c.nomeJogador));
                c.showForm();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(c, "Não foi possível obter o identificador: " + e.getMessage());
            }
        } else {
            c.dispose();
        }
    }

    public ServicoTruco getServico() {
        ServicoTruco s = null;
        try {
            URL url = new URL("http://127.0.0.1:9876/requisicao?wsdl");
            QName qname = new QName("http://comunicacao.si.com.br/", "RequisicaoRemotaService");

            Service ws = Service.create(url, qname);
            s = ws.getPort(ServicoTruco.class);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Não foi possível realizar a conexão com o webservice. \nCertifique-se de ter executado o servidor");
        }
        return s;
    }

    /**
     * insere as mensagens no textArea
     *
     * @param Servico webservice
     */
    private void atualizar(ServicoTruco s) {
        String[] mensagens;
        String mensagemAux;
        String[] cartas;

        String mensagem = s.getMensagens(identificador);

            historicoMensagem = historicoMensagem + mensagem;

            //coloca os comandos disponíveis na text area quando envia o comando .ajuda
            if (mensagem.contains("-> Cria um novo jogo")) {
                tela.textAreaComandosDisponiveis.setText(mensagem);
                mensagem = "";
            }

            if (historicoMensagem.contains("As suas cartas sao")) {

                mensagem = "";
                mensagens = historicoMensagem.split("\\n");

                getServico().executarAcao(identificador, ".dupla");

                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("As suas cartas sao")) {
                        limparCartasTela();
                        
                        mensagemAux = mensagens[i].substring(20).replace("]", "");
                        cartas = mensagemAux.split(",");
                        
                        String carta = cartas[0].trim();
                        tela.btCarta1.setText(carta);
                        
                        if (cartas.length >= 2){
                            carta = cartas[1].trim();
                            tela.btCarta2.setText(carta);
                        }
                        if (cartas.length == 3){
                            carta = cartas[2].trim();
                            tela.btCarta3.setText(carta);
                        }
                    }
                }
                
                habilitarBotoesCartasTela();
            }

            if (tela.textAreaHistorico.getText().contains("Suas cartas")) {

                mensagem = "";
                mensagens = historicoMensagem.split("\\n");
                
                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("Suas cartas")) {
                        limparCartasTela();
                        mensagemAux = mensagens[i].substring(13).replace("]", "");
                        cartas = mensagemAux.split(",");
                        
                        String carta;
                        carta = cartas[0].trim();
                        tela.btCarta1.setText(carta);
                        
                        if (cartas.length >= 2){
                            carta = cartas[1].trim();
                            tela.btCarta2.setText(carta);
                        }
                        if (cartas.length == 3){
                            carta = cartas[2].trim();
                            tela.btCarta3.setText(carta);
                        }
                    }
                }
                
                habilitarBotoesCartasTela();
            }

            if (historicoMensagem.contains("Dupla") && !duplaSetada) {
                mensagens = historicoMensagem.split("\\n");

                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("Dupla") && !mensagens[i].contains("Aceita Dupla1 ou Dupla2")) {
                        mensagem = "";
                        tela.labelDuplaJogador.setText(mensagens[i].substring(5));
                        duplaSetada = true;
                        break;
                    }
                }
            }

            if (historicoMensagem.contains("descartou a carta")) {

                mensagem = "";
                mensagens = historicoMensagem.split("\\n");

                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("descartou a carta")) {
                        getServico().executarAcao(identificador, ".cartas");
                        tela.labelUltimaCartaDescartada.setText(mensagens[i].substring(mensagens[i].length() - 3));
                        break;
                    }
                }
            }

            if (historicoMensagem.contains("Nova rodada iniciada descarte uma carta.")) {
                
                tela.textAreaHistorico.setText("Nova Rodada!\n");
                limparCartasTela();
                habilitarBotoesCartasTela();
                tela.labelUltimaCartaDescartada.setText("***");
                tela.btAceitar.setEnabled(false);
                tela.btAumentar.setEnabled(false);
                historicoMensagem = "";
            }
            
            if (historicoMensagem.contains("Nova mao iniciada.")) {
                
                tela.textAreaHistorico.setText("Nova mão iniciada!\n");
                tela.labelUltimaCartaDescartada.setText("***");
                historicoMensagem = "";
                limparCartasTela();
                habilitarBotoesCartasTela();
            }
            
            if (historicoMensagem.contains("Pontuacao da dupla")) {
                mensagem = "";
                mensagens = historicoMensagem.split("\\n");

                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("Pontuacao da dupla")) {
                        if (mensagens[i].toUpperCase().contains("DUPLA1"))
                            tela.labelPontuacaoDupla1.setText(mensagens[i].substring(mensagens[i].length()-2).trim());
                        else
                            tela.labelPontuacaoDupla2.setText(mensagens[i].substring(mensagens[i].length()-2).trim());
                    }
                }
            }
            
            if (historicoMensagem.contains("trucou")) {
                mensagem = "";
                mensagens = historicoMensagem.split("\\n");

                for (int i = mensagens.length - 1; i >= 0; i--) {
                    if (mensagens[i].contains("trucou")) {
                        tela.labelRodadaTrucada.setEnabled(true);
                        tela.btAumentar.setEnabled(true);
                        tela.btAceitar.setEnabled(true);
                    }
                }
            }
            
            tela.textAreaHistorico.append(mensagem);
    }
    
    private void limparCartasTela(){
        tela.btCarta1.setText("");
        tela.btCarta2.setText("");
        tela.btCarta3.setText("");
    }
    
    private void habilitarBotoesCartasTela(){
        if (tela.btCarta1.getText().equals("")){
            tela.btCarta1.setEnabled(false);
        }
        else{
            tela.btCarta1.setEnabled(true);
        }
        if (tela.btCarta2.getText().equals("")){
            tela.btCarta2.setEnabled(false);
        }
        else{
            tela.btCarta2.setEnabled(true);
        }
        if (tela.btCarta3.getText().equals("")){
            tela.btCarta3.setEnabled(false);
        }
        else{
            tela.btCarta3.setEnabled(true);
        }
    }
    
    private void requisitarPontuacaoAtualizada(){
        executarComandoServidor(".pontuacao Dupla1");
        executarComandoServidor(".pontuacao Dupla2");
    }

    /**
     * Busca as mensagens de um em um segundo no webservice
     */
    private void buscarAtualizacao() {
        final ServicoTruco s = getServico();
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    atualizar(s);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, "Não foi possível atualizar: " + ex.getMessage());
                }
            }
        }
        ).start();
    }
}
