package br.com.si.entidades;

import java.io.Serializable;
import java.util.Objects;

/**
 * Classe que representa uma carta do baralho
 */
public class Carta implements Serializable {

    private final NumeroCarta numero;
    private final Naipe naipe;
    private int valor;

    /**
     *
     * @param numero da carta
     * @param naipe da carta
     */
    public Carta(NumeroCarta numero, Naipe naipe) {
        this.numero = numero;
        this.naipe = naipe;

    }

    public Carta(NumeroCarta numero, Naipe naipe, int valor) {
        this.numero = numero;
        this.naipe = naipe;
        this.valor = valor;
    }

    /**
     * *
     *
     * @return número da carta
     */
    public NumeroCarta getNumero() {
        return numero;
    }

    /**
     *
     *
     * @return naipe da carta
     */
    public Naipe getNaipe() {
        return naipe;
    }

    /**
     * *
     * @return valor inteiro da carta
     */
    public int getValor() {

        return this.valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.numero);
        hash = 17 * hash + Objects.hashCode(this.naipe);
        hash = 17 * hash + this.valor;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carta other = (Carta) obj;
        if (this.valor != other.valor) {
            return false;
        }
        if (this.numero != other.numero) {
            return false;
        }
        return this.naipe == other.naipe;
    }

    @Override
    public String toString() {
        return this.numero.toString() + this.naipe;
    }
}
