package br.com.si.entidades;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Publica os métodos do servidor truco em um webservice soap
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ServicoTruco {

    /**
     * Executa uma das ações no servidor truco
     * *-----------------------------------------------------------------------------;
     * Comandos disponiveis;
     * -----------------------------------------------------------------------------;
     * .criar -> Cria um novo jogo -> ex: .criar jogador vagas; .entrar -> Entra
     * em um novo jogo -> ex: .entrar jogador; .iniciar -> Inicia um jogo criado
     * -> ex: .iniciar; .dupla -> Exibe a sua dupla no jogo -> ex: .dupla;
     * .rodada -> Exibe o numero da rodada atual -> ex: .rodada; .pontuacao ->
     * Exibe a pontuacao de sua dupla -> ex: .pontuacao; .cartas -> Exibe as
     * cartas nao descartadas -> ex: .cartas; .jogadores -> Exibe os nomes dos
     * jogadores -> ex: .jogadores; .descartes -> Exibe as cartas descartadas ->
     * ex: .descartes; .descartar -> Descarta uma carta -> ex: .descartar 4P;
     * .trucar -> Aumenta a aposta para 3 -> ex: .trucar; .aceitar -> Aceita o
     * aumento da aposta -> ex: .aceitar; .correr -> Concede a mao reduzindo a
     * aposta -> ex: .correr; .aumentar -> Aumenta o valor da aposta em 3 -> ex:
     * .aumentar; .sair -> Sai do jogo -> ex: .sair;
     * -----------------------------------------------------------------------------;
     * Parametros;
     * -----------------------------------------------------------------------------;
     * jogador -> Nome do jogador. Aceita qualquer nome; vagas -> Quantidade de
     * vagas no jogo. Aceita 2 ou 4; dupla -> Nome da dupla. Aceita Dupla1 ou
     * Dupla2;
     *
     * @param identificador do jogador
     * @param comando a ser executado
     */
    @WebMethod
    public void executarAcao(Integer identificador, String comando);

    /**
     * Retorna as mensagens de um jogador.
     *
     * @param identificador do jogador
     * @return uma lista de string contendo as mensagens destinadas ao jogador
     */
    @WebMethod
    public String getMensagens(Integer identificador);

    /**
     * Gera um identificador para o jogador para que o controlador possa
     * identificar o jogador em futuras requisições
     *
     * @param nomeJogador nome do jogador
     * @return identificador gerado
     */
    @WebMethod
    public Integer gerarIdentificador(String nomeJogador);
}
