# Projeto 3 - Webservices  

[Como fazer o projeto funcionar](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/wikis/executando-o-projeto)

[Manual do jogador](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/wikis/manual-do-jogador)  

[Adaptações do projeto 1](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/wikis/alteracoes-a-partir-do-projeto-1.)  

[Regras do jogo truco](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/wikis/regras-do-jogo-truco)  

[Documentação](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/wikis/documentacao)  

[Plano de teste](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/blob/DocumentationMariane/Plano_de_Testes.md)
***
