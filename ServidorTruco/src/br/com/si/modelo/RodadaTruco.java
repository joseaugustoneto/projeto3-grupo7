package br.com.si.modelo;

import br.com.si.entidades.Carta;
import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RodadaTruco {

    private final MaoTruco mao;
    private boolean rodadaFinalizada;
    private Dupla duplaVencedora;
    private final Map<Jogador, Boolean> jogadoresDescarteAutorizado = new HashMap<>();
    private final Map<Jogador, Carta> descartesVencedores = new HashMap<>();
    private final Map<Jogador, Carta> descartesJogadores = new HashMap<>();

    /**
     *
     * @return true se a rodada estiver finalizada
     */
    public boolean isRodadaFinalizada() {
        return rodadaFinalizada;
    }

    /**
     * *
     * Controi um objeto rodada de truco
     *
     * @param mao atual da partida de truco
     */
    public RodadaTruco(MaoTruco mao) {
        this.mao = mao;
        mao.getJogo().getVagas().stream().forEach((v) -> {
            jogadoresDescarteAutorizado.put(v.getJogador(), true);
        });
    }

    /**
     * *
     * Valida a possibilidade de um descarte
     *
     * @param jogador que irá descartar a carta
     * @param carta carta a ser descartada
     */
    private void validaDescarte(Jogador jogador, Carta carta) {
        /*Se aumentar a aposta e ninguem aceitar ou correr*/
        if (!mao.isTrucoAutorizado() && mao.getJogadorApostaAceita() == null && mao.getJogadoresDesistentesAposta().size() != 2) {
            throw new ModeloException(String.format("A aposta foi aumentada a dupla %s deve tomar uma decisão.", mao.duplaOposta(mao.getDuplaApostadora())));
        }
        /*Se o jogador não pode descartar então será levantada uma exceção*/
        if (!jogadoresDescarteAutorizado.get(jogador)) {
            throw new ModeloException("Descarte não autorizado!");
        }
        /*A carta selecionada para descarte não existe*/
        if (!mao.getCartasJogadores().get(jogador).contains(carta)) {
            throw new ModeloException(String.format("A carta %1$s não é uma carta do jogador %2$s", carta, jogador));
        }
    }

    private void executaRegrasDaRodada() {

        Carta maiorCarta = descartesJogadores.get(mao.getJogo().getLider());
        for (Map.Entry<Jogador, Carta> e : descartesJogadores.entrySet()) {
            if (maiorCarta.getValor() < e.getValue().getValor()) {
                maiorCarta = e.getValue();
            }
        }

        /*Procurando quais cartas são iguais a maior carta e adiciono no mapa descartesVencedores*/
        for (Map.Entry<Jogador, Carta> e : descartesJogadores.entrySet()) {
            if (maiorCarta.equals(e.getValue())) {
                this.descartesVencedores.put(e.getKey(), e.getValue());
            }
        }

        /*Criando um conjunto de duplas, pois esse só aceita elementos não repetidos
            * Logo se houver um empate esse conjunto conterá duas duplas diferentes já que as cartas de ambas 
            * são iguais
         */
        Set<Dupla> duplasVencedoras = new HashSet<>();
        this.descartesVencedores.entrySet().stream().forEach((e) -> {
            duplasVencedoras.add(mao.duplaDoJogador(e.getKey()));
        });

        /*Há somente uma dupla no conjunto, logo não houve empates, essa dupla  ganhou a rodada.*/
        if (duplasVencedoras.size() == 1) {

            /*Adicionando a dupla vitoriosa ao mapa de rodadas ganhas*/
            duplaVencedora = (Dupla) duplasVencedoras.toArray()[0];
            /*Se houve um empate na rodada a rodadda não foi ganha por ninguém*/
        } else {
            /*Adicionando null ao mapa de rodadas ganhas, indicando um empate*/
            duplaVencedora = null;
        }
    }

    /**
     * *
     * Descarta uma carta da mão
     *
     * @param jogador que irá descartar a carta
     * @param carta carta a ser descartada
     * @throws ModeloException quando não for a vez do jogador descartar
     * @throws ModeloException quando a carta não existir para o jogador
     * especificado
     * @throws ModeloException quando nenhum jogador aceitar um truco ou 6,9,12
     */
    public void descartar(Jogador jogador, Carta carta) {

        validaDescarte(jogador, carta);
        //Remove a carta da mão do jogador pois o mesmo a descartou
        this.mao.getCartasJogadores().get(jogador).remove(carta);
        //Insere a carta na lista de descartes
        this.descartesJogadores.put(jogador, carta);
        //É o último descarte da rodada?        
        if (descartesJogadores.size() == this.mao.getJogo().getVagas().size()) {
            /*Quem venceu a rodada?*/
            executaRegrasDaRodada();
            this.rodadaFinalizada = true;
            mao.defineVencedorMao();
        }
        jogadoresDescarteAutorizado.replace(jogador, false);
    }

    /**
     *
     * @return a dupla vencedora da rodada
     */
    public Dupla getDuplaVencedora() {
        return duplaVencedora;
    }

    /**
     *
     * @return jogadores com descartes autorizados
     */
    public Map<Jogador, Boolean> getJogadoresDescarteAutorizado() {
        return jogadoresDescarteAutorizado;
    }

    /**
     *
     * @return descartes vencedores da jogada
     */
    public Map<Jogador, Carta> getDescartesVencedores() {
        return descartesVencedores;
    }

    /**
     *
     * @return cartas descartadas pelos jogadores na rodada.
     */
    public Map<Jogador, Carta> getDescartesJogadores() {
        return descartesJogadores;
    }
}
