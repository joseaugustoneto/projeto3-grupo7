package br.com.si.modelo;

public class ModeloException extends RuntimeException {

    public ModeloException(String message) {
        super(message);
    }

    public ModeloException(String message, Throwable cause) {
        super(message, cause);
    }
}
