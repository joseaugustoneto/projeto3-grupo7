package br.com.si.modelo;

import br.com.si.entidades.Carta;
import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Representa uma mão em um jogo de truco
 *
 */
public class MaoTruco {

    private final Jogo jogo;
    private final List<RodadaTruco> rodadas = new ArrayList<>();
    private RodadaTruco rodadaAtual;
    private Baralho baralho;
    private boolean trucoAutorizado;
    private Map<Jogador, Boolean> jogadoresCorridaAutorizada = new HashMap<>();
    private Map<Jogador, Boolean> jogadoresAumentoAutorizado = new HashMap<>();
    private Map<Jogador, Boolean> jogadoresAceiteAutorizado = new HashMap<>();
    private final Map<Jogador, List<Carta>> cartasJogadores = new HashMap<>();
    private final List<Jogador> jogadoresDesistentesAposta = new ArrayList<>();
    private int valorAposta = 1;
    private Dupla duplaApostadora;
    private Dupla duplaVencedora;
    private Jogador jogadorApostaAceita;
    private Jogador jogadorDesistenteAposta;

    /**
     * *
     * Método auxiliar que retorna a dupla do jogador
     *
     * @param jogador que será retornado a dupla
     * @return dupla a qual o jogador pertence
     */
    public Dupla duplaDoJogador(Jogador jogador) {
        return getJogo().getJogadorDupla().get(jogador);
    }

    /**
     *
     * @param dupla a qual os jogadores serão retornados
     * @return os jogadores de uma determinada dupla
     */
    public List<Jogador> getJogadoresDaDupla(Dupla dupla) {
        List<Jogador> jogadores = new ArrayList<>();
        getJogo().getVagas().stream().filter((v) -> (v.getDupla().equals(dupla))).forEach((v) -> {
            jogadores.add(v.getJogador());
        });
        return jogadores;
    }

    /**
     * *
     *
     * @param dupla para obter a dupla oposta
     * @return dupla oposta da dupla passada por parâmetro
     */
    public Dupla duplaOposta(Dupla dupla) {
        if (dupla.equals(Dupla.Dupla1)) {
            return Dupla.Dupla2;
        } else {
            return Dupla.Dupla1;
        }
    }

    /**
     * Define se a dupla vencedora da mão caso haja.
     */
    public void defineVencedorMao() {

        duplaVencedora = null;
        /*É a segunda ou terceira rodada?*/
        if (this.rodadas.size() > 1) {

            /*Houve um empate?*/
            if (rodadaAtual.getDuplaVencedora() == null) {
                /* Se a dupla venceu a primeira rodada ela ganha a mão */
                if (rodadas.get(0).getDuplaVencedora().equals(rodadaAtual.getDuplaVencedora())) {
                    /*A dupla ganha a mão*/
                    duplaVencedora = rodadaAtual.getDuplaVencedora();
                }
                /* Não ouve empate na rodada*/
            } else {
                /*Se a dupla adquiriu dois pontos já ganhou a rodada*/
                if (getPontuacaoRodada(rodadaAtual.getDuplaVencedora()) == 2) {
                    duplaVencedora = rodadaAtual.getDuplaVencedora();
                }
                /*Se a primeira empatou quem fez a segunda ganha*/
                if (rodadas.size() == 2) {
                    if (rodadas.get(0).getDuplaVencedora() == null) {
                        duplaVencedora = rodadaAtual.getDuplaVencedora();
                    }
                } else /*Se a terceira rodada foi ganha e as outras duas estavam empatadas então
                      a dupla que fez a terceira leva a mão
                 */ if (rodadas.get(0).getDuplaVencedora() == null && rodadas.get(1).getDuplaVencedora() == null) {
                    duplaVencedora = rodadaAtual.getDuplaVencedora();
                }
            }
        }
        jogo.setFimDeJogo();
    }

    /**
     * Constroi o objeto MaoTruco passando-lhe um jogo
     *
     * @param jogo de truco
     */
    public MaoTruco(Jogo jogo) {
        this.jogo = jogo;
        novaRodada();
        embaralhar();
        darCartas();
        trucoAutorizado = true;
        jogo.getVagas().stream().map((v) -> {
            jogadoresAceiteAutorizado.put(v.getJogador(), false);
            return v;
        }).map((v) -> {
            jogadoresAumentoAutorizado.put(v.getJogador(), false);
            return v;
        }).forEach((v) -> {
            jogadoresCorridaAutorizada.put(v.getJogador(), false);
        });
    }

    /**
     * Aumenta o valor da aposta em 3.valores 6,9,12
     *
     * @param jogador que aumentou a aposta
     */
    public void aumentarAposta(Jogador jogador) {
        if (!jogadoresAumentoAutorizado.get(jogador)) {
            throw new ModeloException("Jogada nao permitida.");
        }
        this.valorAposta = valorAposta + 3;
        /*Indica a dupla que aumentou a aposta*/
        this.duplaApostadora = duplaDoJogador(jogador);
        jogadoresAumentoAutorizado.replace(jogador, false);
        this.jogadoresCorridaAutorizada.replace(jogador, false);
        if (valorAposta + 3 + (getJogo().getPontuacaoDupla(duplaOposta(duplaApostadora))) <= 12) {
            /*Indica os jogadores da dupla que podem aumentar a aposta*/
            getJogadoresDaDupla(duplaOposta(duplaApostadora)).stream().forEach((j) -> {
                this.jogadoresAumentoAutorizado.replace(j, true);
            });
        }

        getJogadoresDaDupla(duplaOposta(duplaApostadora)).stream().forEach((j) -> {
            this.jogadoresAceiteAutorizado.replace(j, true);
            this.jogadoresCorridaAutorizada.replace(j, true);
        });
    }

    /**
     * *
     * Aumenta o valor da aposta para 3
     *
     * @param jogador que trucou.
     */
    public void trucar(Jogador jogador) {
        if (!trucoAutorizado) {
            throw new ModeloException("Jogada nao permitida.");
        }
        this.valorAposta = valorAposta = 3;
        /*Depois que um truca ninguem mais pode trucar. ´So aumentar o valor da aposta*/
        this.trucoAutorizado = false;
        /*Indica a dupla que trucou*/
        this.duplaApostadora = duplaDoJogador(jogador);
        this.jogadoresCorridaAutorizada.replace(jogador, false);

        if (valorAposta + 3 + (getJogo().getPontuacaoDupla(duplaOposta(duplaApostadora))) <= 12) {

            /*Indica os jogadores da dupla que podem aumentar a aposta*/
            getJogadoresDaDupla(duplaOposta(duplaApostadora)).stream().forEach((j) -> {
                this.jogadoresAumentoAutorizado.replace(j, true);

            });
        }

        getJogadoresDaDupla(duplaOposta(duplaApostadora)).stream().forEach((j) -> {
            this.jogadoresAceiteAutorizado.replace(j, true);
            this.jogadoresCorridaAutorizada.replace(j, true);
        });
    }

    /**
     * Aceita a proposta de truco
     *
     * @param jogador que aceitou a aposta
     */
    public void aceitarAposta(Jogador jogador) {
        if (!jogadoresAceiteAutorizado.get(jogador)) {
            throw new ModeloException("Jogada nao permitida.");
        }
        this.jogadorApostaAceita = jogador;
        getJogadoresDaDupla(duplaDoJogador(jogador)).stream().forEach((j) -> {
            this.jogadoresAceiteAutorizado.replace(j, false);
            this.jogadoresCorridaAutorizada.replace(j, false);
            this.jogadoresAumentoAutorizado.replace(j, false);
        });
        getJogadoresDaDupla(duplaOposta(duplaDoJogador(jogador))).stream().forEach((j) -> {
            this.jogadoresAceiteAutorizado.replace(j, false);
            this.jogadoresCorridaAutorizada.replace(j, false);
            this.jogadoresAumentoAutorizado.replace(j, false);
        });
    }

    /**
     * Correr de uma aposta no jogo truco
     *
     * @param jogador que correu do truco
     */
    public void correr(Jogador jogador) {
        if (!jogadoresCorridaAutorizada.get(jogador)) {
            throw new ModeloException("Jogada nao permitida.");
        }
        this.jogadorDesistenteAposta = jogador;
        this.jogadoresDesistentesAposta.add(jogador);
        this.jogadoresCorridaAutorizada.replace(jogador, false);
        this.jogadoresAceiteAutorizado.replace(jogador, false);
        this.jogadoresAumentoAutorizado.replace(jogador, false);
        //Testo se os dois jogadores desistiram do truco, 6, 9,12. Pois se um deles não desistir
        //Ele ainda pode aceitar a aposta 
        if (jogadoresDesistentesAposta.size() == jogo.getVagas().size() / 2) {
            //Quando os dois jogadores correm de um truco, 6, 9, 12 a aposta volta ao valor anterior.     
            this.valorAposta = this.valorAposta - 3;
            duplaVencedora = duplaOposta(duplaDoJogador(jogadoresDesistentesAposta.get(0)));
            rodadaAtual.getDescartesVencedores().clear();
            jogo.setFimDeJogo();
            if (valorAposta == 0) {
                valorAposta = 1;
            }
        }
    }

    /**
     * Embaralha as cartas do baralho
     */
    private void embaralhar() {

        /*Reiniciando o baralho*/
        baralho = new Baralho();
        Collections.shuffle(this.baralho.getCartas());
    }

    /**
     * *
     * Distribui as cartas entre os jogadore 3 por cada jogado Antes de chamar
     * esse método as cartas devem ser embaralhadas
     */
    private void darCartas() {
        /*Distribuindo 3 cartas para cada jogador */
        jogo.getVagas().stream().forEach((v) -> {
            List<Carta> cartasMao = new ArrayList<>(3);
            cartasMao.add(this.baralho.getCartas().remove(0));
            cartasMao.add(this.baralho.getCartas().remove(0));
            cartasMao.add(this.baralho.getCartas().remove(0));
            cartasJogadores.put(v.getJogador(), cartasMao);
        });
    }

    /**
     *
     * @return rodada de truco atual
     */
    public RodadaTruco getRodadaAtual() {
        return rodadaAtual;
    }

    /**
     * inicia uma nova rodada
     *
     * @throws IllegalArgumentException quando chamar o método mais de 3 vezes
     */
    public final void novaRodada() {

        if (rodadas.size() == 3) {
            throw new ModeloException("Não é possivel adicionar uma rodada. Jogo de truco possui no máximo 3.");
        }
        this.rodadaAtual = new RodadaTruco(this);
        this.rodadas.add(this.rodadaAtual);
    }

    /**
     *
     * @param dupla a qual a pontuação pertence
     * @return inteiro de 0-12
     */
    public int getPontuacaoRodada(Dupla dupla) {
        int pontuacao = 0;
        /* Varre a lista de rodadas ao encontrar a dupla passada como parâmetro incrementa a pontuação em 1*/
        pontuacao = rodadas.stream().filter((r) -> (r.getDuplaVencedora() != null && dupla.equals(r.getDuplaVencedora()))).map((_item) -> 1).reduce(pontuacao, Integer::sum);
        return pontuacao;
    }

    /**
     * Indica se uma mão de truco acabou
     *
     * @return true se a mão estiver finalizada
     */
    public boolean maoFinalizada() {
        return duplaVencedora != null;
    }

    /**
     *
     * @return o jogo que a mão pertence
     */
    public Jogo getJogo() {
        return jogo;
    }

    /**
     *
     * @return true se a mão pode ser trucada
     */
    public boolean isTrucoAutorizado() {
        return trucoAutorizado;
    }

    /**
     *
     * @return valor atual da aposta
     */
    public int getValorAposta() {
        return valorAposta;
    }

    /**
     *
     * @return dupla vencedora da mão
     */
    public Dupla getDuplaVencedora() {
        return duplaVencedora;
    }

    /**
     *
     * @return número da rodada
     */
    public int getNumeroRodada() {
        return rodadas.size();
    }

    /**
     *
     * @return baralho truco goiano
     */
    public Baralho getBaralho() {
        return baralho;
    }

    /**
     *
     * @return mapa de jogadores autorizados a correr na mão de truco
     */
    public Map<Jogador, Boolean> getJogadoresCorridaAutorizada() {
        return jogadoresCorridaAutorizada;
    }

    /**
     *
     * @return mapa de jogadores autorizados a aumentar a aposta na mão de truco
     */
    public Map<Jogador, Boolean> getJogadoresAumentoAutorizado() {
        return jogadoresAumentoAutorizado;
    }

    /**
     *
     * @return mapa de jogadores autorizados a aceitar uma aposta de truco
     */
    public Map<Jogador, Boolean> getJogadoresAceiteAutorizado() {
        return jogadoresAceiteAutorizado;
    }

    /**
     *
     * @return mapa de jogadores autorizados a desistir da aposta em uma mão de
     * truco
     */
    public List<Jogador> getJogadoresDesistentesAposta() {
        return jogadoresDesistentesAposta;
    }

    /**
     *
     * @return a dupla que trucou ou aumentou a aposta
     */
    public Dupla getDuplaApostadora() {
        return duplaApostadora;
    }

    /**
     *
     * @return jogador que aceitou a aposta
     */
    public Jogador getJogadorApostaAceita() {
        return jogadorApostaAceita;
    }

    /**
     *
     * @return jogador qeu desistiu da aposta
     */
    public Jogador getJogadorDesistenteAposta() {
        return jogadorDesistenteAposta;
    }

    /**
     *
     * @return mapa de cartas da mão do jogador
     */
    public Map<Jogador, List<Carta>> getCartasJogadores() {
        return cartasJogadores;
    }
}
