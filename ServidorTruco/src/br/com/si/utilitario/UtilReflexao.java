package br.com.si.utilitario;

import java.lang.reflect.*;

/**
 * Classe utilitaria. Possui métodos staticos para lidar com a reflexão no Java.
 *
 */
public class UtilReflexao {

    /**
     *
     * Invoca um método via reflexão.
     *
     * @param classe para localizar o método
     * @param nomeMetodo nome do método a ser invocado
     * @param parametros Classes dos argumentos para invocar o método
     * @param argumentos Argumentos para a invocação do método
     * @param instancia instancia do objeto para invocação
     *
     * @return objeto como resultado da invocação ou null caso o método tenha
     * retorno void
     *
     */
    public static Object invocarMetodo(Class classe, String nomeMetodo, Class[] parametros,
            Object[] argumentos, Object instancia) {
        Object retorno = null;
        /* Invocando o método  */
        try {
            Method m = classe.getDeclaredMethod(nomeMetodo, parametros);
            retorno = m.invoke(instancia, argumentos);

        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return retorno;
    }
}
