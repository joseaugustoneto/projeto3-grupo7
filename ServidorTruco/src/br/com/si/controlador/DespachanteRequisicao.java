package br.com.si.controlador;

import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import br.com.si.modelo.Jogo;
import br.com.si.modelo.MaoTruco;
import br.com.si.modelo.RodadaTruco;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

/**
 * Preenche o objeto mensagem a ser enviado ao cliente com base nas informações
 * da mensagem de requisição.
 */
public class DespachanteRequisicao {

    private final ControladorTruco controlador;

    public DespachanteRequisicao(ControladorTruco controlador) {
        this.controlador = controlador;
    }

    /**
     * Cria uma nova mesa de truco
     *
     * @param ip do requisitante
     */
    public void criar(String ip) {
        /*Escritor do usuário requisitante*/
        TrucoPrintStream p = controlador.escritorSaidaPorIP(ip);
        p.println("Jogo criado. Aguardando jogadores.");
    }

    /**
     * Envia um broadCast excluindo um ip
     *
     * @param mensagem mensagem a ser enviada
     * @param ipsExcluidos ips que não receberão a mensagem
     */
    private void broadCastExclusivo(String mensagem, String... ipsExcluidos) {

        controlador.getCanaisDeSaida().entrySet().stream().forEach((e) -> {
            for (String s : ipsExcluidos) {
                if (!s.equals(e.getKey())) {
                    controlador.escritorSaidaPorIP(e.getKey()).println(mensagem);
                }
            }
        });
    }

    /**
     * Envia um broadCast
     *
     * @param mensagem mensagem a ser enviada
     */
    private void broadCast(String mensagem) {
        controlador.getCanaisDeSaida().entrySet().stream().forEach((e) -> {
            if (e.getValue().getStream() != null) {
                new PrintStream(e.getValue().getStream()).println(mensagem);
            } else {
                e.getValue().getMensagens().add(mensagem);
            }
        });
    }

    /**
     * Imprime a resposta do método entrarJogo
     *
     * @param ip do requisitante
     * @param parametros da ação
     */
    public void entrar(String ip, Map<String, Object> parametros) {
        /*escritor de saída do requisitante*/
        TrucoPrintStream p = controlador.escritorSaidaPorIP(ip);
        p.println("Voce entrou no jogo, aguardando inicio.");

        Jogo jogo = controlador.getJogo();
        List<Jogador> jogadores = jogo.getJogadores();

        /*Removendo o próprio jogador da lista*/
        jogadores.remove(jogo.getJogadores().indexOf(ControladorTruco.obterInstancia().getJogador(ip, parametros)));
        p.println(String.format("Os jogadores sao %s", jogadores));

        /*Enviando a mensagem a todos exceto ao requisitante*/
        broadCastExclusivo(String.format("O jogador %s entrou no jogo", controlador.getJogador(ip, parametros).getNome()), ip);
    }

    /**
     * Imprime a resposta do método iniciar
     */
    public void iniciar() {

        broadCast("O jogo foi iniciado.");
        broadCast("Cartas embaralhadas.");

        Jogo jogo = controlador.getJogo();
        MaoTruco maoAtual = jogo.getMaoAtual();
        controlador.getCanaisDeSaida().entrySet().stream().forEach((e) -> {
            String mensagem = String.format("As suas cartas sao %s", maoAtual.getCartasJogadores().get(jogo.getJogadorPorIp(e.getKey())));
            if (e.getValue().getStream() != null) {
                new PrintStream(e.getValue().getStream()).println(mensagem);
            } else {
                e.getValue().getMensagens().add(mensagem);
            }
        });
    }

    /**
     * Imprime a resposta do método descartar
     *
     * @param ip do requisitante
     * @param parametros da ação
     */
    public void descartar(String ip, Map<String, Object> parametros) {
        /*printStream do usuario requisitante*/
        TrucoPrintStream p = controlador.escritorSaidaPorIP(ip);
        p.println(String.format("Voce descartou a carta %s", parametros.get("carta")));

        /*broadcast excluindo o usuario requisitante*/
        broadCastExclusivo(String.format("O jogador %1$s descartou a carta %2$s", controlador.getJogador(ip, parametros), parametros.get("carta")), ip);
        RodadaTruco rodadaAtual = controlador.getJogo().getMaoAtual().getRodadaAtual();

        /*Testando se os descartes vencedores foram preenchidos se sim então a rodada acabou*/
        if (rodadaAtual.isRodadaFinalizada()) {
            broadCast(String.format("Descartes dessa rodada %s", rodadaAtual.getDescartesJogadores()));
            broadCast(String.format("Descartes vencedores %s", rodadaAtual.getDescartesVencedores()));
        }
        if (controlador.getJogo().getMaoAtual().maoFinalizada()) {
            Dupla duplaVencedora = controlador.getJogo().getMaoAtual().getDuplaVencedora();
            broadCast(String.format("%1$s:%2$s venceu a mao", duplaVencedora, controlador.getJogo().getJogadoresPorDupla(duplaVencedora)));
        }
        if (controlador.getJogo().isFimDeJogo()) {
            Dupla duplaVencedora = controlador.getJogo().getDuplaVencedora();
            broadCast(String.format("%1$s:%2$s atingiu a pontuacao 12 e venceu o jogo. Bye Bye...", duplaVencedora, controlador.getJogo().getJogadoresPorDupla(duplaVencedora)));
        }
    }

    /**
     * Imprime a resposta do método correr
     *
     * @param jogador requisitante
     */
    public void correr(Jogador jogador) {
        /*printStream do usuario requisitante*/
        Jogador jogadorRequisitante = jogador;
        Jogador jogadorParceiro = controlador.getJogo().getJogadorParceiro(jogadorRequisitante);

        int valorAposta = controlador.getJogo().getMaoAtual().getValorAposta() + 3;
        String nomeAposta = valorAposta == 3 ? "truco" : (valorAposta == 6 ? "seis" : (valorAposta == 9 ? "nove" : "doze"));

        /*printStream do usuario requisitante*/
        TrucoPrintStream requisitante = controlador.escritorSaidaPorIP(jogadorRequisitante.getIp());
        requisitante.println(String.format("Voce desistiu do %s", nomeAposta));

        if (jogadorParceiro != null) {
            /*printStream do parceiro do jogador requisitante*/
            TrucoPrintStream parceiro = controlador.escritorSaidaPorIP(jogadorParceiro.getIp());
            parceiro.println(String.format("Seu parceiro desistiu do %s", nomeAposta));
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s desistiu do %2$s", jogadorRequisitante, nomeAposta), jogadorParceiro.getIp(), jogadorRequisitante.getIp());
        } else {
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s desistiu do %2$s", jogadorRequisitante, nomeAposta), jogadorRequisitante.getIp());
        }
    }

    /**
     * Imprime a resposta do método trucar
     *
     * @param jogador requisitante
     */
    public void trucar(Jogador jogador) {

        Jogador jogadorRequisitante = jogador;
        Jogador jogadorParceiro = controlador.getJogo().getJogadorParceiro(jogadorRequisitante);

        /*printStream do usuario requisitante*/
        TrucoPrintStream requisitante = controlador.escritorSaidaPorIP(jogadorRequisitante.getIp());
        requisitante.println("Voce trucou a mao");

        /*Se o jogador tem um parceiro*/
        if (jogadorParceiro != null) {
            /*printStream do parceiro do jogador requisitante*/
            TrucoPrintStream parceiro = controlador.escritorSaidaPorIP(jogadorParceiro.getIp());
            parceiro.println("Seu parceiro trucou a mao");
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %s trucou a mao voce pode correr aceitar ou aumentar", jogadorRequisitante), jogadorParceiro.getIp(), jogadorRequisitante.getIp());
        } else {
            broadCastExclusivo(String.format("O jogador %s trucou a mao voce pode correr aceitar ou aumentar", jogadorRequisitante), jogadorRequisitante.getIp());
        }
    }

    /**
     * Imprime a resposta do método trucar
     *
     * @param jogador requisitante
     */
    public void aceitar(Jogador jogador) {

        Jogador jogadorRequisitante = jogador;
        Jogador jogadorParceiro = controlador.getJogo().getJogadorParceiro(jogadorRequisitante);

        int valorAposta = controlador.getJogo().getMaoAtual().getValorAposta();
        String nomeAposta = valorAposta == 3 ? "truco" : (valorAposta == 6 ? "seis" : (valorAposta == 9 ? "nove" : "doze"));

        /*printStream do usuario requisitante*/
        TrucoPrintStream requisitante = controlador.escritorSaidaPorIP(jogadorRequisitante.getIp());
        requisitante.println(String.format("Voce aceitou o %s", nomeAposta));

        if (jogadorParceiro != null) {
            /*printStream do parceiro do jogador requisitante*/
            TrucoPrintStream parceiro = controlador.escritorSaidaPorIP(jogadorParceiro.getIp());
            parceiro.println(String.format("Seu parceiro aceitou o %s", nomeAposta));
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s aceitou o %2$s descarte uma carta", jogadorRequisitante, nomeAposta), jogadorParceiro.getIp(), jogadorRequisitante.getIp());
        } else {
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s aceitou o %2$s descarte uma carta", jogadorRequisitante, nomeAposta), jogadorRequisitante.getIp());
        }
    }

    /**
     * Imprime a resposta do método aumentar
     *
     * @param jogador requisitante
     */
    public void aumentar(Jogador jogador) {

        Jogador jogadorRequisitante = jogador;
        Jogador jogadorParceiro = controlador.getJogo().getJogadorParceiro(jogadorRequisitante);

        int valorAposta = controlador.getJogo().getMaoAtual().getValorAposta();
        String nomeAposta = valorAposta == 3 ? "truco" : (valorAposta == 6 ? "seis" : (valorAposta == 9 ? "nove" : "doze"));

        /*printStream do usuario requisitante*/
        TrucoPrintStream requisitante = controlador.escritorSaidaPorIP(jogadorRequisitante.getIp());
        requisitante.println(String.format("Voce pediu %s", nomeAposta));

        if (jogadorParceiro != null) {
            /*printStream do parceiro do jogador requisitante*/
            TrucoPrintStream parceiro = controlador.escritorSaidaPorIP(jogadorParceiro.getIp());
            parceiro.println(String.format("Seu parceiro pediu %s", nomeAposta));
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s pediu  %2$s voce pode aceitar, correr ou aumentar", jogadorRequisitante, nomeAposta), jogadorParceiro.getIp(), jogadorRequisitante.getIp());
        } else {
            /*broadcast excluindo o usuario requisitante e seu parceiro*/
            broadCastExclusivo(String.format("O jogador %1$s pediu  %2$s voce pode aceitar, correr ou aumentar", jogadorRequisitante, nomeAposta), jogadorRequisitante.getIp());
        }
    }

    /**
     * Imprime a resposta do método novaMao
     */
    public void novaRodada() {
        /*Ação executada com sucesso*/
        broadCast("Nova rodada iniciada descarte uma carta.");
    }

    /**
     * Imprime a resposta do método novaRodada
     */
    public void novaMao() {

        /*Envia a mesma mensagem a todos os usuarios*/
        broadCast("Nova mao iniciada.");
        broadCast("Cartas embaralhadas.");

        Jogo jogo = controlador.getJogo();
        MaoTruco maoAtual = jogo.getMaoAtual();

        /*Envia a mensagem com as cartas de cada usuário*/
        controlador.getCanaisDeSaida().entrySet().stream().forEach((e) -> {
            String mensagem = String.format("As suas cartas sao %s", maoAtual.getCartasJogadores().get(jogo.getJogadorPorIp(e.getKey())));
            if (e.getValue().getStream() != null) {
                new PrintStream(e.getValue().getStream()).println(mensagem);
            } else {
                e.getValue().getMensagens().add(mensagem);
            }
        });
    }

    /**
     * Imprime na tela do usuario os comandos disponíveis no jogo
     *
     * @param ip do requisitante
     */
    public void ajuda(String ip) {

        TrucoPrintStream p = controlador.escritorSaidaPorIP(ip);
        p.println(".criar       \t-> Cria um novo jogo                \t\t-> ex: .criar  jogador vagas");
        p.println(".entrar      \t-> Entra em um novo jogo            \t-> ex: .entrar jogador");
        p.println(".iniciar     \t-> Inicia um jogo criado            \t\t-> ex: .iniciar");
        p.println(".dupla       \t-> Exibe a sua dupla no jogo        \t-> ex: .dupla");
        p.println(".rodada      \t-> Exibe o numero da rodada atual   \t-> ex: .rodada");
        p.println(".pontuacao   \t-> Exibe a pontuacao de sua dupla   \t-> ex: .pontuacao");
        p.println(".cartas      \t-> Exibe as cartas nao descartadas  \t-> ex: .cartas");
        p.println(".jogadores   \t-> Exibe os nomes dos jogadores     \t-> ex: .jogadores");
        p.println(".descartes   \t-> Exibe as cartas descartadas      \t-> ex: .descartes");
        p.println(".descartar   \t-> Descarta uma carta               \t\t-> ex: .descartar 4P");
        p.println(".trucar      \t-> Aumenta a aposta para 3          \t-> ex: .trucar");
        p.println(".aceitar     \t-> Aceita o aumento da aposta       \t-> ex: .aceitar");
        p.println(".correr      \t-> Concede a mao reduzindo a aposta \t-> ex: .correr");
        p.println(".aumentar    \t-> Aumenta o valor da aposta em 3   \t-> ex: .aumentar");
        p.println(".sair        \t-> Sai do jogo                      \t\t-> ex: .sair");
        p.println("-----------------------------------------------------------------------------");
        p.println("Parametros");
        p.println("-----------------------------------------------------------------------------");
        p.println("jogador      -> Nome do jogador. Aceita qualquer nome");
        p.println("vagas        -> Quantidade de vagas no jogo. Aceita 2 ou 4");
        p.println("dupla        -> Nome da dupla. Aceita Dupla1 ou Dupla2");
    }

    /**
     * Imprime na tela a dupla do jogador
     *
     * @param ip do requisitante
     */
    public void dupla(String ip) {
        controlador.escritorSaidaPorIP(ip).println(String.format("%s",
                controlador.getJogo().getJogadorDupla().get(controlador.getJogador(ip, null))));
    }

    /**
     * Imprime na o número da rodada atual
     *
     * @param ip do requisitante
     */
    public void rodada(String ip) {
        controlador.escritorSaidaPorIP(ip).println(String.format("Rodada atual: %s",
                controlador.getJogo().getMaoAtual().getNumeroRodada()));
    }

    /**
     * Imprime a pontuação de uma dupla
     *
     * @param jogador requisitante
     * @param dupla passada como parâmetro
     */
    public void pontuacao(Jogador jogador, Dupla dupla) {
        controlador.escritorSaidaPorIP(jogador.getIp()).
                println(String.format("Pontuacao da dupla DUPLA1%1$s: %2$s ", dupla,
                        controlador.getJogo().getPontuacaoDupla(dupla)));
    }

    /**
     * Imprime as cartas atuais do jogador
     *
     * @param jogador requisitante
     */
    public void cartas(Jogador jogador) {
        controlador.escritorSaidaPorIP(jogador.getIp()).println(String.format("Suas cartas %s ",
                controlador.getJogo().getMaoAtual().getCartasJogadores().get(jogador)));
    }

    /**
     * Imprime o nome dos jogadores do jogo
     *
     * @param jogador requisitante
     */
    public void jogadores(Jogador jogador) {
        controlador.escritorSaidaPorIP(jogador.getIp()).println(String.format("Jogadores: %s ",
                controlador.getJogo().getJogadores()));
    }

    /**
     * Imprime os descartes realizados na rodada
     *
     * @param jogador requisitante
     */
    public void descartes(Jogador jogador) {
        controlador.escritorSaidaPorIP(jogador.getIp()).println(String.format("Descartes: %s ",
                controlador.getJogo().getMaoAtual().getRodadaAtual().getDescartesJogadores()));
    }

    /**
     * Imprime a notificação que o jogador saiu e finaliza o jogo
     *
     * @param jogador requisitante
     */
    public void sair(Jogador jogador) {
        controlador.escritorSaidaPorIP(jogador.getIp()).println("Voce deixou o jogo.");
        broadCastExclusivo(String.format("O jogador %s deixou o jogo.", jogador), jogador.getIp());
        broadCastExclusivo("Jogo finalizado.", jogador.getIp());
        broadCastExclusivo(String.format("Dupla vencedora: %1$s %2$s",
                controlador.getJogo().getDuplaVencedora(),
                controlador.getJogo().getJogadoresPorDupla(controlador.getJogo().getDuplaVencedora())),
                jogador.getIp());
    }
}
