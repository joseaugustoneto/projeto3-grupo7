package br.com.si.controlador;

import br.com.si.comunicacao.Servidor;
import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import br.com.si.modelo.Jogo;
import br.com.si.modelo.ModeloException;
import br.com.si.utilitario.UtilReflexao;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controla todas as requisições dos clientes disponibilizando: Criação de mesas
 * de truco. Consultas de mesas de truco. Acesso aos controlodares de mesas
 *
 */
public class ControladorTruco {

    /* Variável de instancia do controlador*/
    private static ControladorTruco controladorPrincipal;
    private static ControladorTruco controladorWebService;
    private List<String> mesas;
    /* Mapa de controladores do jogo truco por mesa*/
    private final Map<String, Jogo> modelos = new HashMap<>();
    private final Map<String, Jogador> jogadores = new HashMap<>();
    private final Map<String, ArrayList<String>> mensagens = new HashMap<>();
    private final List<OuvinteServico> ouvintes = new ArrayList<>();
    private final String[] comandos = {".dupla", ".rodada", ".pontuacao", ".cartas", ".jogadores", ".descartes", ".novaMao", ".novaRodada", ".ajuda", ".criar", ".entrar", ".iniciar", ".descartar", ".trucar", ".aceitar", ".correr", ".aumentar", ".sair"};
    private final int[] parametros = {0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0};
    private final String[] nomeParametros = {"", "", "dupla", "", "", "", "", "", "", "jogador,vagas", "jogador", "", "carta", "", "", "", "", ""};
    private final DespachanteRequisicao saida = new DespachanteRequisicao(this);
    private Integer identificador = 0;

    /**
     * *
     * Construtor do controlador principal
     */
    private ControladorTruco() {
        ouvintes.add(new GerenciadorTruco(this));
    }

    /*Métodos auxiliares*/
    /**
     * Retornar o jogador parametrizado no objeto de mensagem.
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @return jogador que solicitou a requisição.
     */
    public Jogador getJogador(String ip, Map<String, Object> parametros) {
        Jogador j = jogadores.get(ip);
        if (j == null) {
            j = new Jogador(parametros.get("jogador").toString(), ip);
            jogadores.put(ip, j);
            mensagens.put(ip, new ArrayList<>());
        }
        return j;
    }

    /**
     * Retornar o nome da mesa parametrizado no objeto mensagem.
     *
     * @return nome da mesa.
     */
    public String getNomeDaMesa() {
        return "mesa1";
    }

    /**
     * Retornar o jogo do usuário da requisição.
     *
     * @return o jogo do usuário da requisição.
     */
    public Jogo getJogo() {
        Jogo jogo = modelos.get("mesa1");
        if (jogo == null) {
            throw new ModeloException("Jogo inexistente");
        }
        return jogo;
    }

    /**
     * Cria uma nova mesa de truco
     *
     * @param ip do executante
     * @param parametros da ação
     */
    public void criar(String ip, Map<String, Object> parametros) {
        modelos.put("mesa1", new Jogo("mesa1", getJogador(ip, parametros), Integer.valueOf(parametros.get("vagas").toString())));
        saida.criar(ip);
    }

    /**
     *
     * @param ip para obtenção do stream
     * @return stream de saída de um ip
     */
    public TrucoPrintStream escritorSaidaPorIP(String ip) {
        TrucoPrintStream p = null;
        try {
            if (!Servidor.getClientesConectados().isEmpty()) {
                p = new TrucoPrintStream(new PrintStream(Servidor.getClientesConectados().get(ip)), mensagens.get(ip));
            } else {
                p = new TrucoPrintStream(mensagens.get(ip));
            }
            return p;
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return p;
    }

    /**
     *
     * @return canais de saída desse jogo
     */
    public Map<String, TrucoPrintStream> getCanaisDeSaida() {
        Map<String, TrucoPrintStream> streams = new HashMap<>();
        //Implementação via webservice
        if (controladorWebService != null) {

            mensagens.entrySet().stream().forEach((e) -> {
                streams.put(e.getKey(), new TrucoPrintStream(e.getValue()));
            });
            //Implementação via socket    
        } else {
            Servidor.getClientesConectados().entrySet().stream().forEach(
                    (e) -> {
                        streams.put(e.getKey(), new TrucoPrintStream(e.getValue(), mensagens.get(e.getKey())));
                    }
            );
        }
        return streams;
    }

    /**
     *
     * @param mensagem exceção gerada pelo modelo
     * @param ip requisitante ao qual será impresso o erro
     */
    private void imprimeErro(String mensagem, String ip) {
        escritorSaidaPorIP(ip).println(mensagem);
    }

    /**
     *
     * @param comando passado ao controlador
     * @return a ação de um comando
     */
    private String getAcao(String comando) {
        return comando.split(" ")[0].replace(".", "");
    }

    /**
     *
     * @param comando passado ao controlador
     * @return os parâmetros da ação
     */
    private Map<String, Object> getParametrosAcao(String comando) {

        Map<String, Object> parametrosAcao = new HashMap<>();
        int posicaoComando = getPosicaoComando("." + getAcao(comando));
        if (!"".equals(nomeParametros[posicaoComando])) {
            int i = 0;
            for (String c : nomeParametros[posicaoComando].split(",")) {
                parametrosAcao.put(c, comando.split(" ")[++i]);
            }
        }
        return parametrosAcao;
    }

    /**
     *
     * @param comando para procurar a posição no vetor
     * @return a posição do comando no vetor
     */
    private int getPosicaoComando(String comando) {
        int i;
        for (i = 0; i < comandos.length; i++) {
            if (comando.equals(comandos[i])) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Valida um comando passado pelo usuário
     *
     * @param comando digitado pelo usuário
     */
    private void validaComando(String comando) {
        for (int i = 0; i < comandos.length; i++) {
            if (comandos[i].equals(comando.split(" ")[0])) {
                if (comando.split(" ").length - 1 == parametros[i]) {
                    return;
                }
            }
        }
        throw new ModeloException("Comando incorreto.");
    }

    /**
     * *
     *
     * @param ip cliente requisitante
     * @param comando comando enviado pelo cliente
     */
    public void servico(String ip, String comando) {
        try {
            validaComando(comando);
            UtilReflexao.invocarMetodo(this.getClass(), getAcao(comando), new Class[]{String.class, Map.class

            }, new Object[]{ip, getParametrosAcao(comando)}, this);
            disparaEvento(ip, getAcao(comando), getParametrosAcao(comando));

        } catch (RuntimeException e) {
            /*Testa se a exceção é de negócio*/
            if (e.getCause() instanceof InvocationTargetException) {
                if (controladorWebService != null) {
                    throw new RuntimeException(((InvocationTargetException) e.getCause()).getTargetException().getMessage());
                }
                imprimeErro(((InvocationTargetException) e.getCause()).getTargetException().getMessage(), ip);
                /*Se a exceção não é de negócio não chegará ao usuário*/
            } else if (e instanceof ModeloException) {
                if (controladorWebService != null) {
                    throw new RuntimeException(e.getMessage());
                }
                imprimeErro(e.getMessage(), ip);
            } else {
                imprimeErro("Erro inesperado", ip);
            }
        }
    }

    /**
     * *
     * Método que fabria uma instancia única do controlador principal
     *
     * @return ControladorPrincipal
     */
    public static ControladorTruco obterInstancia() {

        if (controladorPrincipal == null) {
            controladorPrincipal = new ControladorTruco();
        }
        return controladorPrincipal;
    }

    /**
     * *
     * Método que fabria uma instancia única do controlador principal
     *
     * @return ControladorPrincipal
     */
    public static ControladorTruco obterInstanciaWebService() {

        if (controladorWebService == null) {
            controladorWebService = new ControladorTruco();
        }
        return controladorWebService;
    }

    /**
     * *
     * Chamar o método de descarte no modelo passando o jogador e a carta
     * parametrizados.
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void descartar(String ip, Map<String, Object> parametros) {
        Jogo jogo = getJogo();
        /*Executa método de negócio*/
        jogo.getMaoAtual().getRodadaAtual().descartar(getJogador(ip, parametros), jogo.getMaoAtual().getBaralho().getCartaPorNome(parametros.get("carta").toString()));
        /*Imprime saída*/
        saida.descartar(ip, parametros);

    }

    /**
     * *
     * Inicia o jogo no modelo
     *
     * @param ip do requisitante
     * @param parametros da ação
     *
     * @see Jogo
     */
    public void iniciar(String ip, Map<String, Object> parametros) {
        /*Executa método de negócio*/
        getJogo().iniciar(getJogador(ip, parametros));
        /*Imprime saída*/
        saida.iniciar();
    }

    /**
     * *
     * Chama o método de sair jogo no modelo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     *
     * @see Jogo
     */
    public void sair(String ip, Map<String, Object> parametros) {
        /*Executa método de negócio*/
        getJogo().sairDoJogo(getJogador(ip, parametros));
        /*Imprime a saída*/
        saida.sair(getJogador(ip, parametros));
    }

    /**
     * *
     * Chama o método de entrar no jogo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     *
     * @see Jogo
     */
    public void entrar(String ip, Map<String, Object> parametros) {
        /*Executa método de negócio*/
        getJogo().entrarNoJogo(getJogador(ip, parametros));
        /*Imprime saída*/
        saida.entrar(ip, parametros);
    }

    /**
     * Chama o método aumentarAposta no modelo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     *
     * @see Jogo
     */
    public void aumentar(String ip, Map<String, Object> parametros) {
        getJogo().getMaoAtual().aumentarAposta(getJogador(ip, parametros));
        saida.aumentar(getJogador(ip, parametros));
    }

    /**
     * Chama o método trucar no modelo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void trucar(String ip, Map<String, Object> parametros) {
        getJogo().getMaoAtual().trucar(getJogador(ip, parametros));
        saida.trucar(getJogador(ip, parametros));
    }

    /**
     * Chama o método aceitarAposta no modelo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void aceitar(String ip, Map<String, Object> parametros) {
        getJogo().getMaoAtual().aceitarAposta(getJogador(ip, parametros));
        saida.aceitar(getJogador(ip, parametros));
    }

    /**
     * Chama o método correr no modelo passando o jogador da requisição
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void correr(String ip, Map<String, Object> parametros) {
        getJogo().getMaoAtual().correr(getJogador(ip, parametros));
        saida.correr(getJogador(ip, parametros));
    }

    /**
     * Chama o método novaMao no modelo
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void novaMao(String ip, Map<String, Object> parametros) {
        if (!ip.equals("gerenciador")) {
            throw new ModeloException("Jogada nao autorizada.");
        }
        //Executa método de negócio
        getJogo().novaMao();
        //Envia resposta ao usuário
        saida.novaMao();
    }

    /**
     * Chama o método novaRodada no modelo
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void novaRodada(String ip, Map<String, Object> parametros) {
        if (!ip.equals("gerenciador")) {
            throw new ModeloException("Jogada nao autorizada.");
        }
        //Executa método de negócio
        getJogo().getMaoAtual().novaRodada();
        //Envia a resposta ao usuário
        saida.novaRodada();
    }

    /**
     * Imprime a lista de comandos ao usuário
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void ajuda(String ip, Map<String, Object> parametros) {
        saida.ajuda(ip);
    }

    /**
     * Imprime a dupla do jogador
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void dupla(String ip, Map<String, Object> parametros) {
        saida.dupla(ip);
    }

    /**
     * Imprime O número da rodada atual
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void rodada(String ip, Map<String, Object> parametros) {
        saida.rodada(ip);
    }

    /**
     * Imprime A pontuação da dupla passada como parâmetro
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void pontuacao(String ip, Map<String, Object> parametros) {
        Dupla dupla = null;
        try {
            dupla = Dupla.valueOf(parametros.get("dupla").toString().substring(0, 1).toUpperCase() + parametros.get("dupla").toString().substring(1).toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new ModeloException("Dupla inválida.", e.getCause());
        }
        saida.pontuacao(getJogador(ip, parametros), dupla);
    }

    /**
     * Imprime A cartas atuais do jogador
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void cartas(String ip, Map<String, Object> parametros) {
        saida.cartas(getJogador(ip, parametros));
    }

    /**
     * Imprime Os jogadores do jogo
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void jogadores(String ip, Map<String, Object> parametros) {
        saida.jogadores(getJogador(ip, parametros));
    }

    /**
     * Imprime os descartes da rodada
     *
     * @param ip do requisitante
     * @param parametros da ação
     * @see Jogo
     */
    public void descartes(String ip, Map<String, Object> parametros) {
        saida.descartes(getJogador(ip, parametros));
    }

    /**
     * Dispara o evento de ações para os observadores
     *
     * @param ip do requisitante
     * @param acao executada
     * @param parametros da ação
     */
    public void disparaEvento(String ip, String acao, Map<String, Object> parametros) {
        /*Notifica os observadores que uma ação foi executada*/
        this.ouvintes.stream().forEach((o) -> {
            o.servicoExecutado(ip, acao);
        });
        if (!acao.equals("ajuda") && this.getJogo().isFimDeJogo()) {
            modelos.put("mesa1", null);
        }
    }

    /**
     *
     *
     * @param nomeJogador nome do jogador
     * @return a identificação do jogador
     */
    public Integer getIdentificador(String nomeJogador) {
        identificador++;
        Map<String, Object> p = new HashMap<>();
        p.put("jogador", nomeJogador);
        getJogador(identificador.toString(), p);
        return identificador;
    }

    /**
     *
     * @return as mensagens de um determinado jogador
     */
    public Map<String, ArrayList<String>> getMensagens() {
        return mensagens;
    }
}
