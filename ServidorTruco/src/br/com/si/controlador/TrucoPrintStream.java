package br.com.si.controlador;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

public class TrucoPrintStream {

    private final List<String> mensagens;
    private OutputStream stream;

    public TrucoPrintStream(List<String> mensagens) {
        this.mensagens = mensagens;
    }

    public TrucoPrintStream(OutputStream out, List<String> mensagens) {
        this.stream = out;
        this.mensagens = mensagens;
    }

    public void println(String l) {
        if (stream != null) {
            new PrintStream(stream).println(l);
            mensagens.add(l);
        } else {
            mensagens.add(l);
        }
    }

    public List<String> getMensagens() {
        return mensagens;
    }

    public OutputStream getStream() {
        return stream;
    }
}
